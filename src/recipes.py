# This is just a convenient way of visualizing how the recipe data structure should look.
# Most likely this will move to a database or some other kind of file-structure.

recipes = {
    "pizzas": [
        {
            "uid": "lilla-napoli",
            "name": "Neapolitan pizza à Lilla Napoli",
            "flour": 1,
            "water": 0.66,
            "salt": 0.028,
            "yeast": 0.00025,
            "work_time_in_minutes": 130,
            "total_time_in_minutes": 1560,
            "description":
                "You are going to make {{ NUM_OF_PIZZAS }} {{ NAME }}.\n"
                "\n"
                "Ingredients:\n"
                "- Flour: {{ TOTAL_FLOUR|int }}{{ UNIT }} \n"
                "- Water (15C): {{ TOTAL_WATER|int }}{{ UNIT }}\n"
                "- Salt:  {{ '%.2f'|format(TOTAL_SALT) }}{{ UNIT }}\n"
                "- Fresh yeast: {{ '%.2f'|format(TOTAL_YEAST) }}{{ UNIT }}\n"
                "\n"
                "Go about:\n"
                "- Dissolve the yeast in a little bit of the water, in a big bowl."
                "When fully dissolved pour in the rest of the water.\n"
                "- Dissolve the salt in the same water.\n"
                "- Pour in the flour in the water."
                "Work the dough with your hands or a spatula until its fully integrated.\n"
                "- Let the dough rest for 30 minutes with a cover.\n"
                "- Fold the dough 4-6 times with the 'French folding' technique. Let it rest for 15 minutes.\n"
                "- Do the above 2 more times.\n"
                "{% if EXTRA_DOUGH_WEIGHT > 0 %}"
                "- Pinch {{ EXTRA_DOUGH_WEIGHT }}{{ UNIT }} from the dough. "
                "Do a small french fold, and put it in a rain meter."
                "It should be at about {{ 12 / 80 * EXTRA_DOUGH_WEIGHT }} cm.\n"
                "{% endif %}"
                "- Now let the dough rest for about 12 hours in room temperature (ca. 21C).\n"
                "\n"
                "- Create small balls (with french folding technique) of à {{DOUGH_WEIGHT}}{{ UNIT }}"
                " and put them in small plastic jars.\n"
                "- Let them rest in 12 more hours."
                "{% if EXTRA_DOUGH_WEIGHT > 0 %}"
                " When the rain meter is about {{ 25 / 80 * EXTRA_DOUGH_WEIGHT }} cm then the dough is ready."
                "{% endif %}"
                "\n\n"
                " - Now put whatever you want on your pizza and bake it at about 400-450 degrees (C)."
                "\n -- Congratulation to your '{{ NAME }}' dough! --\n",
            "description_format": "jinja"
        },
        {
            "uid": "dummy-uid",
            "name": "dummy-name",
            "flour": 1,
            "water": 0.6875,
            "salt": 0.03125,
            "yeast": 0.0125,
            "description":
                "Neapolitan Overnight: ...\n\n"
                "tot. flour: {{ TOTAL_FLOUR|int }} \n"
                "tot. water: {{ TOTAL_WATER|int }} \n"
                "tot. salt:  {{ '%.2f'|format(TOTAL_SALT) }} \n"
                "tot. yeast: {{ '%.2f'|format(TOTAL_YEAST) }} \n"
                "num_of_pizzas: {{ NUM_OF_PIZZAS }} \n"
                "dough_weight: {{ DOUGH_WEIGHT }} \n"
                "lef_dough_weight: {{ EXTRA_DOUGH_WEIGHT }} \n",
            "description_format": "jinja"
        },
    ],
}
