from model import Recipe


def main(pizzas: dict[str, Recipe], args):
    recipe = pizzas[args.uid]
    recipe.print_recipe(args.num_of_pizzas, args.dough_weight, args.extra_dough_weight)
