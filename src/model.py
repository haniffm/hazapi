import math
import sys
from enum import Enum

from pydantic import BaseModel


class DescriptionFormat(str, Enum):
    TEXT = "text"
    JINJA = "jinja"


FORMATTING_EXCLUDED_KEYS = ('description',)


class Recipe(BaseModel):
    uid: str
    name: str
    flour: float
    water: float
    salt: float
    yeast: float
    unit: str = "g"
    description: str = None
    description_format: DescriptionFormat = DescriptionFormat.TEXT

    class Config:
        allow_mutation = False

    def _sum_all_parts(self):
        return self.flour + self.water + self.salt + self.yeast

    def _get_multiplier(self, num_of_pizzas, dough_weight, extra_dough_weight):
        return math.ceil((num_of_pizzas * dough_weight) / self._sum_all_parts()) + extra_dough_weight

    def get_recipe(self, num_of_pizzas, dough_weight, extra_dough_weight) -> str:
        multiplier = self._get_multiplier(num_of_pizzas, dough_weight, extra_dough_weight)

        total_flour = self.flour * multiplier
        total_water = self.water * multiplier
        total_salt = self.salt * multiplier
        total_yeast = self.yeast * multiplier

        if self.description_format == DescriptionFormat.JINJA:
            from jinja2 import Template
            tm = Template(self.description)
            rendered_desc = tm.render(
                **{k.upper(): v for k, v in vars(self).items() if k not in FORMATTING_EXCLUDED_KEYS},
                TOTAL_FLOUR=total_flour,
                TOTAL_WATER=total_water,
                TOTAL_SALT=total_salt,
                TOTAL_YEAST=total_yeast,
                NUM_OF_PIZZAS=num_of_pizzas,
                DOUGH_WEIGHT=dough_weight,
                EXTRA_DOUGH_WEIGHT=extra_dough_weight
            )
        elif self.description_format == DescriptionFormat.TEXT:
            rendered_desc = self.description
        else:
            print("Unknown Description format, setting description to empty.", file=sys.stderr)
            rendered_desc = ""

        return rendered_desc

    def print_recipe(self, num_of_pizzas, dough_weight, extra_dough_weight):
        rendered_desc = self.get_recipe(num_of_pizzas, dough_weight, extra_dough_weight)
        print(rendered_desc)
