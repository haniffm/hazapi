#!/usr/bin/env bash

CUR_PATH=$(readlink -e ./src)
export PATH=$CUR_PATH:$PATH
eval "$(register-python-argcomplete hazapi)"
