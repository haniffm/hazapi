# Hazapi
A command line tool to generate pizza recipes given the amount of pizza and the recipe type.

## Pre-requisites
- Python 3.9+

## Quick Start

Create virtual python environment:
```shell
python3.9 -m venv .venv
```

Activate the virtual environment:
```shell
source .venv/bin/activate
```

Install python libraries:
```shell
pip install -r requirements.txt
```

Run it (will give you my favorite pizza recipe):
```shell
./src/hazapi
```

## Customize recipe
The command line tool gives you some options which will dynamically change the recipe(s).

For example if you only want to make 2 pizzas, do like this:
```shell
./src/hazapi --num-of-pizzas 2
```

And if you want a smaller pizza, do like this:
```shell
./src/hazapi --dough-weight 180
```

To list all the options, do:
```shell
./src/hazapi --help
```

